const bcrypt = require("bcrypt");
const User = require("../models/user.modal.js");
const jwt = require("jsonwebtoken");

module.exports.signup_post = async (req, res) => {
  try {
    const hashedPassword = await bcrypt.hash(req.body.password, 10);
    const token = jwt.sign(
      {
        name: req.body.name,
        email: req.body.email,
      },
      process.env.ACCESS_TOKEN_SECRET
    );
    await User.create({
      username: req.body.userName,
      email: req.body.email,
      password: hashedPassword,
      token: token,
    });
    res.json({
      status: 200,
      msg: "Successfull Registered",
    });
  } catch (err) {
    console.log(err);
    res.json({
      data: "",
      status: 500,
      msg: "User with this email already registered!",
    });
  }
};

module.exports.login_post = async (req, res) => {
  const user = await User.findOne({
    email: req.body.email,
  });

  if (user === null) {
    return res.send({ status: 400, msg: "Cannot find any user." });
  }

  try {
    if (await bcrypt.compare(req.body.password, user.password)) {
      const accessToken = jwt.sign(
        {
          name: user.name,
          email: user.email,
        },
        process.env.ACCESS_TOKEN_SECRET
      );
      await User.updateOne(
        { email: user.email },
        { $set: { token: accessToken } }
      );

      user.save();
      res.json({
        accessToken: accessToken,
        status: 200,
        msg: "Successfull login",
      });
    } else res.json({ msg: "Invalid Password", status: 401 });
  } catch (err) {
    res.status(500).json({ msg: "Error", data: err });
  }
};
