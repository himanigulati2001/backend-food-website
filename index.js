const express = require("express");
const authRoutes = require("./Routes/AuthRoutes");
const app = express();
const cors = require("cors");
const dotnet = require("dotenv");
dotnet.config();
//middleware
app.use(cors());
app.use(express.json());

//routes
app.use(authRoutes);

const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/Authentication");

app.listen(3001, () => console.log("App Listening"));

// function authenticateToken(req, res, next) {
//   const authHeader = req.headers["Authorization"];
//   const token = authHeader && authHeader.split(" ")[1];
//   const dbToken = User.find({ email: req.body.email });
//   console.table(token, dbToken);
//   if (token === null) return res.sendStatus(401);
//   jwt.verify(token, dbToken, (err, user) => {
//     if (err) return res.sendStatus(403);
//     req.user = user;
//     next();
//   });
// }

// app.get("/user", authenticateToken, (req, res) => {
//   const user = User.find({ email: req.user.email });
//   res.json(user);
//   // return res.send({ users: users });
// });
