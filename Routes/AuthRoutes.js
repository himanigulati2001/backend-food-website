const { Router } = require("express");
const router = Router();

const authController = require("../Controllers/authController");
const authenticateUser = require("../Middleware/auth");

router.post("/signup", authController.signup_post);
router.post("/login", authController.login_post);
router.get("/verify-user", authenticateUser, (req, res) => res.send("testing"));
module.exports = router;
